var ChobiWiki_Node = function(name,options){
    this.name = name;

    if(options.level){
        this.level = options.level;
    }

    if(options.text){
        this.text = options.text;
    }
    
    if(options.children){
        this.children = options.children;
    }
    if(options.style){
        this.style = options.style;
    }
    if(options.option){
        this.option = options.option;
    }
}
ChobiWiki_Node.prototype = {
    getName: function(){
        return this.name;
    }
}
var ChobiWiki_Writer_Html = function(){
/*
    if(options.pages){
        this.pages = options.pages;
    }
*/
}
ChobiWiki_Writer_Html.prototype = {
    footnote: [],
    parser: undefined,

    register: function(parser){
        this.parser = parser;
    },
    apply_style: function(tag, style){
        var result = "<" + tag;
        var styles = [];
        for(var key in style){
            var name = key.replace(/([A-Z])/,"-$1").toLowerCase();
            styles.push(name + ":" + style[key])
        }
        if(styles.length){
            result += " style=\"" + styles.join(";") + "\"";
        }
        result += ">";
        
        return result;
    },
    write_section: function(obj){
        return "<h" + obj.level + ">" + this.escapeHtml(obj.text) + "<a name=\"" + encodeURI(obj.text) + "\">†</a></h" + obj.level + ">";
    },
    write_pre: function(obj){
        return "<pre>" + this.escapeHtml(obj.text) + "</pre>";
    },
    write_paragraph: function(obj){
        return "<p>" + this.inline(obj.text) + "</p>";
    },
    write_horizontal_rule: function(obj){
        return "<hr />";
    },
    write_blockquote: function(obj){
        return "<blockquote><div>" + this.escapeHtml(obj.text) + "</div></blockquote>";
    },
    write_line_break: function(obj){
        return "";
    },
    write_table: function(obj){
        var result = "";
        result += "<table>";
        var length = obj.children.length;
        for(var n = 0; n < length; n++){
            if(obj.children[n].option == "h"){
                result += "<thead>";
            }
            
            result += "<tr>";
            
            var xlength = obj.children[n].children.length;
            for(var x = 0; x < xlength; x++){
                if(obj.children[n].children[x].option.head){
                    result += "<th>";
                    result += this.inline(obj.children[n].children[x].text);
                    result += "</th>";
                }else{
                    result += this.apply_style("td",obj.children[n].children[x].option.style);
                    result += this.inline(obj.children[n].children[x].text);
                    result += "</td>";
                }
            }
            
            result += "</tr>";

            if(obj.children[n].option == "h"){
                result += "</thead>";
            }
        }
        result += "</table>";
        return result;
    },
    write_definition_list: function(obj){
        return this.sub_write_definition_list(obj);
    },
    sub_write_definition_list: function(obj){
        var result = "";
        if(obj.children){
            //console.log(obj.style);
            result += "<dl>";

            var length = obj.children.length;
            for(var n = 0; n < length; n++){
                if(obj.children[n].children.length > 0){
                    result += "<dd>";
                    result += this.sub_write_definition_list(obj.children[n]);
                    result += "</dd>\n";
                }else{
                    if(obj.children[n].getName() == "definition_list"){
                        //console.log("children::" +obj.children[n].children);
                        result += "<dd>";
                        result += this.sub_write_definition_list(obj.children[n]);
                        result += "</dd>\n";
                    }else if(obj.children[n].getName() == "definition_description"){
                        result += "<dd>";
                        if(obj.children[n].text){
                            result += this.trim(this.inline(obj.children[n].text));
                        }
                        result += "</dd>\n";
                    }else if(obj.children[n].getName() == "definition_term"){
                        result += "<dt>";
                        if(obj.children[n].text){
                            result += this.trim(this.inline(obj.children[n].text));
                        }
                        result += "</dt>\n";
                    }
                }
            }
            result += "</dl>";
        }else {
        }
        return result;
    },
    write_list: function(obj){
        //console.log(require("util").inspect(obj,false,null));
        return this.sub_write_list(obj);
    },
    sub_write_list: function(obj){
        var result = "";
        if(obj.children){
            //console.log(obj.style);
            if(obj.style =="bullet"){
                result += "<ul>";
            }else{
                result += "<ol>";
            }

            var length = obj.children.length;
            for(var n = 0; n < length; n++){
                if(obj.children[n].children.length > 0){
                    result += "<li>";
                    result += this.sub_write_list(obj.children[n]);
                    result += "</li>\n";
                }else{
                    if(obj.children[n].getName() == "list"){
                        //console.log("children::" +obj.children[n].children);
                        result += "<li>";
                        result += this.sub_write_list(obj.children[n]);
                        result += "</li>\n";
                    }else if(obj.children[n].getName() == "list_item"){
                        result += "<li>";
                        if(obj.children[n].text){
                            result += this.trim(this.inline(obj.children[n].text));
                        }
                        result += "</li>\n";
                    }
                }
            }
            if(obj.style =="bullet"){
                result += "</ul>";
            }else{
                result += "</ol>";
            }
        }else {
        }
        return result;
    },
    ex_write_contents: function(nodes){
        var result = [];
        var level = 0;
        var diff = 0;
        var moe = [];
        //result.;
        var length = nodes.length;
        for(var i = 0; i < length; i++){
            var obj = nodes[i];

            if(obj == undefined){
                continue;
            }else if(obj.getName() == "section"){
                if(obj.level > level) {
                    //result.push("\n");
                    diff = obj.level - level;

                    for(level; level < obj.level; level++){
                        result.push("<ul>");
                        if(diff > 1){
                          result.push("<li>");
                          diff--;
                        }
                    }
                } else if(obj.level < level){
                    result.push("</ul>");
                    diff = level - obj.level;

                    for(level; obj.level < level  ; level--){
                        result.push("</a></li>");

                        if(diff > 1){
                          result.push("</a></li>");
                          diff--;
                        }
                    }

                    if(moe.length > 0){
                        moe.shift();
                        result.push("</ul>");
                    }
                } else if(obj.level == level) {
                    if(moe.length > 0){
                        moe.shift();
                        result.push("</a></li>");
                    }
                }
                
                result.push("<li><a href=\"#" + encodeURI(obj.text.replace(/^\s+|\s+$/g,"")) + "\">");
                result.push(obj.text.replace(/^\s+|\s+$/g,""));
                //result.push("</li>\n");
                moe.push(1);
            }
        }
        moe.shift();
        moe.shift();
        //ulをたたむ
        if(level > 0 ){
            if(moe.length > 0){
                moe.shift();
                result.push("</a></li>");
            }

            diff = level;
            for(level; level > 0 ; level--){
                result.push("</ul>");

                if(diff > 1){
                  result.push("</a></li>");
                  diff--;
                }
            }

            if(moe.length > 0){
                moe.shift();
                result.push("</a></li>");
            }
        }



        return result.join("");
    },
    
    write: function(nodes){
        this.footnote = [];
        var result = "";
        var length = nodes.length;
        for(var i = 0; i < length; i++){
            var obj = nodes[i];

            if(obj == undefined){
                continue;
            }else if(obj.getName() == "section"){
                result += this.write_section(obj);
            }else if(obj.getName() == "pre"){
                result += this.write_pre(obj);
            }else if(obj.getName() == "paragraph"){
                result += this.write_paragraph(obj);
            }else if(obj.getName() == "list"){
                result += this.write_list(obj);
            }else if(obj.getName() == "table"){
                result += this.write_table(obj);
            }else if(obj.getName() == "horizontal_rule"){
                result += this.write_horizontal_rule(obj);
            }else if(obj.getName() == "line_break"){
                result += this.write_line_break(obj);
            }else if(obj.getName() == "definition_list"){
                result += this.write_definition_list(obj);
            }else if(obj.getName() == "blockquote"){
                result += this.write_blockquote(obj);
            }else if(obj.getName() == "contents"){
                result += this.ex_write_contents(nodes);
            }
        }
        
        if(this.footnote.length > 0){
            result += "<hr />";
            result += "<ul>";
            for(var i = 0; i < this.footnote.length; i++){
                result += "<li><a name=\"note_" +(i+1) +"\"><span style=\"color:red\">*" + (i+1) + "</span></a>" + this.footnote[i] + "</li>";
            }
            result += "</ul>";
            result += "<hr />";
        }
        
        return result;
    },

    trim: function(text){
        return text.replace(/^\s+|\s+$/g,"");
    },

    escapeHtml: function(text){
        if(!text){
            return "";
        }
        return text.replace(/&/g,"&amp;").replace(/"/g,"&quot;").replace(/'/g,"&#039;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
    },
    
    inline: function(text){
        var parser = this;
        var result = text;
        if(!result){
            return "";
        }

        result = result.replace(/\(\((.+?)\)\)/g,function(match,text){
            var result = "<a href=\"#note_" + (parser.footnote.length+1) + "\">*" + (parser.footnote.length+1) + "</a>";
            parser.footnote.push(text);
            return result;
        });
        result = result.replace(/'''(.+?)'''/g,"<em>$1</em>");
        result = result.replace(/''(.+?)''/g,"<b>$1</b>");
        result = result.replace(/%%(.+?)%%/g,"<s>$1</s>");
        // InterWiki / Link
        result = result.replace(/\[\[(.+?):(.+?)\]\]/,function(match,name,src){
            return "<a href=\"" + src + "\">" + name + "</a>";
        });

        // Alias
        result = result.replace(/\[\[(.+?)>(.+?)\]\]/,function(match,name,src){
            return "<a href=\"" + src + "\">" + name + "</a>";
        });

        // link
        result = result.replace(/\[\[(.+?)\]\]/,function(match,name){
            return "<a href=\"" + name + "\">" + name + "</a>";
        });
        
        // WikiName
        result = result.replace(/[A-Z][a-z]+[A-Z][a-z][a-zA-Z]+/,function(match){

            return "<span style='color:#999999'>" + match + "?</span>";
        });
        result = result.replace(/&([a-zA-Z]+)(.*)?;/g,function(match,type,options){
            if(parser["inline_"+type]){
                return parser["inline_"+type](match,options);
            }else{
                return match;
            }
        });
        return result;
    },
    inline_ref: function(text, option){
        var src = option.substr(1,option.length-2);
        if(src.match(/(png|jpg|gif)$/)){
            return "<img src=\"" + src + "\" />";
        }
    },
    inline_version: function(){
        return "<b>ChobiWiki_Writer_Html version: 0.1</b>";
    },
    inline_t: function(){
        return "\t";
    },
    inline_br: function(){
        return "<br />";
    },
    inline_now: function(){
        return new Date().toString();
    },
    inline_color: function(all,option){
        option = option.match(/\(.+?\)/g);
        var colors = option[0].replace(/^\(/,"").replace(/\)$/,"");
        
        if(colors.indexOf(",")){
            var tmp = colors.split(",");
            color = tmp[0];
            back = tmp[1];
        }else{
            color = colors;
        }
        if(color){
            color = "color:" + color;
        }
        if(back){
            back = "background-color:" + back;
        }
        
        return "<span style=\"" +
                color + ";" + back +
                "\">" + 
                option[1].replace(/^\(/,"").replace(/\)$/,"") +
                "</span>";
    }
}

////////////////////////////////////////////////////////////////////////


/**
 * 新Parserはここから下
 */
var ChobiWiki_Parser_DefinitionList = function(){
}

ChobiWiki_Parser_DefinitionList.prototype = {
    priority: 3,
    stack: [],
    mark: ":",
    level: 0,
    MAX_LEVEL: 3,

    match: function(line){
        if( line.charAt(0) == this.mark && line.match(/:.+?\|.*/)){
            var level  = 0;
            var length = line.length;
            for(var i = 0; i < length; i++){
                if(line.charAt(i) == this.mark) {
                    level++;
                } else {
                    break;
                }
            }

            if(level > this.MAX_LEVEL){
                return false;
            }else {
                this.level = level;
                return true;
            }
        }else{
            return false;
        }
    },
    
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        this.stack.push({
            level: this.level,
            text: line.substr(this.level),
        });
        this.level = 0;
        return "";
    },
    
    hasStack: function(){
        return (this.stack.length > 0);
    },
    
    flush: function(){

        lists = this.stack;
        delete this.stack;

        return new ChobiWiki_Node("definition_list",{
            children: this.sub_flush(lists,1),
            style: this.style,
            level: 1});
    },
        
    sub_flush: function(list, level){
        var children = [];
        var diff = 0;
        
        while(obj = list.shift()){
            diff = (level - obj.level)*-1;
            var definitions = obj.text.split("|");
            
            if(diff > 0){
                //積み直す
                if(diff == 2){
                    lists.unshift(obj);
                    var tmp = [];
                    tmp.push(new ChobiWiki_Node("definition_list",{
                        level: level+diff,
                        style: this.style,
                        children: this.sub_flush(lists,obj.level),
                        }
                    ));

                    children.push(new ChobiWiki_Node("definition_list",{
                        level: level+diff-1,
                        style: this.style,
                        children: tmp
                    }));
                } else {
                    lists.unshift(obj);
                    children.push(new ChobiWiki_Node("definition_list",{
                        level: level+diff,
                        style: this.style,
                        children: this.sub_flush(lists,obj.level),
                        }
                    ));
                }
            }else if(diff < 0){
                //レベルが下がったので積みなおして返す
                lists.unshift(obj);
                return children;
            }else{
                children.push(new ChobiWiki_Node("definition_term",{text:definitions[0],children:[]}));
                children.push(new ChobiWiki_Node("definition_description",{text:definitions[1],children:[]}));
            }
        }
        return children;
    }
}


var ChobiWiki_Parser_BulletList = function(){
}

ChobiWiki_Parser_BulletList.prototype = {
    priority: 3,
    stack: [],
    mark: "-",
    level: 0,
    MAX_LEVEL: 3,
    style: "bullet",

    match: function(line){
        if( line.charAt(0) == this.mark){
            var level  = 0;
            var length = line.length;
            for(var i = 0; i < length; i++){
                if(line.charAt(i) == this.mark) {
                    level++;
                } else {
                    break;
                }
            }

            if(level > this.MAX_LEVEL){
                return false;
            }else {
                this.level = level;
                return true;
            }
        }
    },
    
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        this.stack.push({
            level: this.level,
            text: line.substr(this.level),
        });
        this.level = 0;
        return "";
    },
    
    hasStack: function(){
        return (this.stack.length > 0);
    },
    
    flush: function(){

        lists = this.stack;
        delete this.stack;

        return new ChobiWiki_Node("list",{
            children: this.sub_flush(lists,1),
            style: this.style,
            level: 1});
    },
        
    sub_flush: function(list, level){
        var children = [];
        var diff = 0;
        
        while(obj = list.shift()){
            diff = (level - obj.level)*-1;
            
            if(diff > 0){
                //積み直す
                if(diff == 2){
                    lists.unshift(obj);
                    var tmp = [];
                    tmp.push(new ChobiWiki_Node("list",{
                        level: level+diff,
                        style: this.style,
                        children: this.sub_flush(lists,obj.level),
                        }
                    ));

                    children.push(new ChobiWiki_Node("list",{
                        level: level+diff-1,
                        style: this.style,
                        children: tmp
                    }));
                } else {
                    lists.unshift(obj);
                    children.push(new ChobiWiki_Node("list",{
                        level: level+diff,
                        style: this.style,
                        children: this.sub_flush(lists,obj.level),
                        }
                    ));
                }
            }else if(diff < 0){
                //レベルが下がったので積みなおして返す
                lists.unshift(obj);
                return children;
            }else{
                children.push(new ChobiWiki_Node("list_item",{text:obj.text,children:[]}));
            }
        }
        return children;
    }
}

var ChobiWiki_Parser_OrderedList = function(){};
ChobiWiki_Parser_OrderedList.prototype = new ChobiWiki_Parser_BulletList();
ChobiWiki_Parser_OrderedList.prototype.mark = "+";
ChobiWiki_Parser_OrderedList.prototype.style = "ordered";



var ChobiWiki_Parser_BlockQuote = function(){
    const MAX_LEVEL = 3;
}
ChobiWiki_Parser_BlockQuote.prototype = {
    priority: 3,
    stack: [],
    level: 0,

    match: function(line){
        if(line.charAt(0) == ">") {
            var length = line.length;
            for(var i = 0; i < length; i++){
                if(i > this.MAX_LEVEL){
                    return false;
                }
                if(line.charAt(i) == ">") {
                    this.level++;
                } else {
                    break;
                }
            }

            return true;
        }
    },
        
    parse: function(lines, pointer){
        var line = lines[pointer.offset];

        this.stack.push({
            level: this.level,
            text: line.substr(this.level),
        });
        this.level = 0;

        return "";
    },

    hasStack: function(){
        return (this.stack.length > 0);
    },

    flush: function(){
        var result = [];
        while(obj = this.stack.shift()){
            result.push(obj.text);
        }

        return new ChobiWiki_Node("blockquote",{
            text: result.join("")
        });
    },
    
}



var ChobiWiki_Parser_Table = function(){}

ChobiWiki_Parser_Table.prototype = {
    stack: [],
    max_column: 0,

    match: function(line){
        if(line.charAt(0) == "|"){
            return true;
        }
    },
    
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        var cell = [];
        var length = line.length;
        var tmp = [];
        var result = [];
        var option = "";

        for(var i = 0; i < length; i++){
            if(line.charAt(i) == "|"){
                if(tmp.length > 0){
                    result.push(tmp.join(""));
                    tmp = [];
                }
            } else {
                tmp.push(line.charAt(i));
            }
        }
        if(tmp.length > 0){
            option = tmp.join("").replace(/^\s+|\s+$/g,"").charAt(0);
        }

        this.max_column = Math.max(this.max_column, result.length);
        this.stack.push({
            columns: result,
            option: option,
        });
        return "";
    },
    
    flush: function(){
        var result = [];
        
        var rows = [];

        for(var i in this.stack){
            var columns = [];
            for(var n in this.stack[i].columns){
                var txt = this.stack[i].columns[n].replace(/^\s+|\s+$/g,"");
                var opt = {style:{},head:false};
                
                if(txt.indexOf(":") >= 0){
                    var list = txt.split(":");
                    var tmp = [];
                    var d;
                    while(d = list.shift()){
                        if(d == "CENTER"){
                            opt.style.textAlign = "center";
                        }else if(d == "LEFT"){
                            opt.style.textAlign = "left";
                        }else if(d == "RIGHT"){
                            opt.style.textAlign = "right";
                        }else if(d.match(/^COLOR\(.+?\)/)){
                              opt.style.color = d.match(/\((.+?)\)/)[1];
                        }else if(d.match(/^BGCOLOR\(.+?\)/)){
                              opt.style.backgroundColor = d.match(/\((.+?)\)/)[1];
                        }else if(d.match(/^SIZE\(.+?\)/)){
                              opt.style.width = d.match(/\((.+?)\)/)[1] + "px";
                        }else{
                              tmp.push(d);
                        }
                    }
                    txt = tmp.join(":");
                }

                if(txt.match(/^~/)){
                    txt = txt.replace(/^~/,'');
                    opt.head = true;
                }
                
                columns.push(new ChobiWiki_Node("table_cell",{
                    text: txt,
                    option: opt,
                }));
            }

            rows.push(new ChobiWiki_Node("table_row",{
                children: columns,
                option: this.stack[i].option,
            }));
        }

        this.max_column = 0;
        this.stack = [];
        return new ChobiWiki_Node("table",{
            children: rows
        });
    },
    

    hasStack: function(){
        return (this.stack.length > 0);
    },
}



var ChobiWiki_Parser_Paragraph = function(){
}

ChobiWiki_Parser_Paragraph.prototype = {

    match: function(line){
        if(!line.match(/^\s+$/)){
            return true;
        }
    },

    parse: function(lines, pointer){
        var line = this.sub_parse(lines,pointer);

        return new ChobiWiki_Node("paragraph",{
            text:line
        });
    },
    sub_parse: function(lines, pointer){
        var line = lines[pointer.offset];
        if(line.match(/~$/)){
            line = line.replace(/~$/,"\n");
            pointer.offset++;
            if(lines[pointer.offset]){
                line += this.sub_parse(lines,pointer);
            }
        }
        return line;
    },
        
    hasStack: function(){
        return false;
    },
}

var ChobiWiki_Parser_Horizon = function(){
}
ChobiWiki_Parser_Horizon.prototype = {
    priority: 3,

    match: function(line){
        if(line.match(/^-{4,}$/)) {
            return true;
        }
    },
        
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        return new ChobiWiki_Node("horizontal_rule",{});
    },
        
    hasStack: function(){
        return false;
    },
}


var ChobiWiki_Parser_LineBreak = function(){
}
ChobiWiki_Parser_LineBreak.prototype = {
    priority: 3,

    match: function(line){
        if(line.match(/^\s*$/)) {
            return true;
        }
    },
        
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        return new ChobiWiki_Node("line_break",{})
    },
        
    hasStack: function(){
        return false;
    },
}



var ChobiWiki_Parser_SimpleTable = function(){}

ChobiWiki_Parser_SimpleTable.prototype = {
    stack: [],
    max_column: 0,

    match: function(line){
        if(line.charAt(0) == ","){
            return true;
        }
    },
    
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        var cell = [];
        var length = line.length;
        var tmp = [];
        var result = [];
        var option = "";

        for(var i = 0; i < length; i++){
            if(line.charAt(i) == ","){
                if(tmp.length > 0){
                    result.push(tmp.join(""));
                    tmp = [];
                }
            } else {
                tmp.push(line.charAt(i));
            }
        }
        if(tmp.length > 0){
            result.push(tmp.join(""));
        }

        this.max_column = Math.max(this.max_column, result.length);
        this.stack.push({
            columns: result,
            option: option,
        });

        return "";
    },
    
    flush: function(){
        
        var rows = [];
        for(var i in this.stack){
            var columns = [];

            for(var n in this.stack[i].columns){
                columns.push(new ChobiWiki_Node("table_cell",{
                    text: this.stack[i].columns[n],
                    option: {head:false,style:{}},
                }));
            }

            rows.push(new ChobiWiki_Node("table_row",{
                children: columns,
            }));

        }

        this.max_column = 0;
        this.stack = [];
        return new ChobiWiki_Node("table",{
            children: rows
        });
    },
    

    hasStack: function(){
        return (this.stack.length > 0);
    },
}


var ChobiWiki_Parser_Comment = function(){
}
ChobiWiki_Parser_Comment.prototype = {
    priority: 3,

    match: function(line){
        if(line.charAt(0) == "/" && line.charAt(1) == "/") {
            return true;
        }
    },
        
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        return "";
    },
        
    hasStack: function(){
        return false;
    },
}


var ChobiWiki_Parser_Preformat = function(){
}
ChobiWiki_Parser_Preformat.prototype = {
    priority: 3,
    stack: [],

    match: function(line){
        if(line.charAt(0) == " " || line.charAt(0) == "\t") {
            return true;
        }
    },
        
    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        this.stack.push(line.substr(1));
        return "";
    },

    hasStack: function(){
        return (this.stack.length > 0);
    },

    flush: function(){
        var result = [];
        while(text = this.stack.shift()){
            result.push(text);
        }
        
        return new ChobiWiki_Node("pre",{
            text: result.join("\n"),
        });
    }
}



var ChobiWiki_Parser_Section = function(){
}
ChobiWiki_Parser_Section.prototype = {
    priority: 3,
    level: 0,
    MAX_LEVEL: 4,

    match: function(line){
        if( line.charAt(0) == "*"){
            
            var length = line.length;
            for(var i = 0; i < length; i++){
                if(line.charAt(i) == "*") {
                    this.level++;
                } else {
                    break;
                }
            }
            
            if(this.level > this.MAX_LEVEL){
                this.level = 0;
                return false;
            }

            return true;
        }
    },
    
    hasStack: function(){
        return false;
    },

    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        var level = this.level;
        this.level = 0;

        return new ChobiWiki_Node("section",{
            level: level,
            text:  line.substr(level).replace(/\[.+?\]/,"").replace(/^\s+|\s+$/g,""),
        });
    },
}

var ChobiWiki_Parser_Ex_Contents = function(){
}
ChobiWiki_Parser_Ex_Contents.prototype = {
    priority: 3,

    match: function(line){
        if(line == "#contents"){
            return true;
        }
    },
    
    hasStack: function(){
        return false;
    },

    parse: function(lines, pointer){
        var line = lines[pointer.offset];
        var level = this.level;
        this.level = 0;

        return new ChobiWiki_Node("contents",{});
    },
}


var ChobiWiki = function(writer){
    this.writer = writer;
    writer.register(this);
    this.register();
};

ChobiWiki.prototype = {
    parser: [],

    register: function(){
        this.parser.push(new ChobiWiki_Parser_Comment());
        this.parser.push(new ChobiWiki_Parser_Section());
        this.parser.push(new ChobiWiki_Parser_Preformat());
        this.parser.push(new ChobiWiki_Parser_Table());
        this.parser.push(new ChobiWiki_Parser_SimpleTable());
        this.parser.push(new ChobiWiki_Parser_DefinitionList());
        this.parser.push(new ChobiWiki_Parser_BulletList());
        this.parser.push(new ChobiWiki_Parser_OrderedList());
        this.parser.push(new ChobiWiki_Parser_BlockQuote());
        this.parser.push(new ChobiWiki_Parser_LineBreak());
        this.parser.push(new ChobiWiki_Parser_Horizon());
        this.parser.push(new ChobiWiki_Parser_Ex_Contents());
        this.parser.push(new ChobiWiki_Parser_Paragraph());
    },

    convertHTML: function(message){
        var result = [];
        var lines = message.split("\n");
        var last = null;
        var length = lines.length;
        var pointer = {offset:0};
        
        for(pointer.offset = 0; pointer.offset<length; pointer.offset++){
            for(var x in this.parser){
                if(this.parser[x].match(lines[pointer.offset])){
                    var parser = this.parser[x];
                    last = this.parser[x];

                    var pom = parser.parse(lines,pointer);

                    if(pom instanceof ChobiWiki_Node){
                        result.push(pom);
                    }

                    if(!(parser instanceof ChobiWiki_Parser_Paragraph)){
                        delete this.parser[x];
                        this.parser.unshift(parser);
                    }
                    break;
                }
                
                if (last && last.hasStack()){
                    delete last;
                    result.push(last.flush());
                }
            }
        }

        if (parser && parser.hasStack()){
            result.push(parser.flush());
        }

        return this.writer.write(result);
    }
}
var value = "> fdsafdsafdsa\n"+
            "World\n" +
            "Uhi";

var converter = new ChobiWiki(new ChobiWiki_Writer_Html());
console.log(converter.convertHTML(value));
