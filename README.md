# ChobiWiki - PukiWiki like javascript parser & renderer -

  var wiki = new ChobiWiki(new ChobiWiki_Writer_Html());
  process.stdout.write(wiki.parse("contents"));

# License

MIT License

# Authors

Shuhei Tanuma

# Dependencies

nothing.